<?php
use yii\helpers\Html;
/** @var yii\web\View $this */

$this->title = 'Noticias';
?>
<style>
/*-------------------------------------CSS Noticias--------------------------------------------------------*/
body {
    font-family: 'Arial', sans-serif;
    background-color: #f8f8f8;
    color: #333;
    margin: 0;
    padding: 0;
}

.site-index {
    width: 80%;
    margin: auto;
}

.jumbotron {
    padding: 30px;
}

.specialnews {
    color: #333;
    font-size: 36px;
}

.article-background {
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 8px;
    padding: 20px;
    margin-bottom: 20px;
}

.font-replace {
    font-family: 'Times New Roman', serif;
}

.especialnoticias {
    margin-bottom: 30px;
}

.especialnoticias img {
    max-width: 100%;
    border-radius: 8px;
    margin-bottom: 15px;
}

.texto8 {
    color: #888;
}

/* Responsividad para dispositivos móviles */
@media screen and (max-width: 768px) {
    .site-index {
        width: 90%;
    }
}


/*-------------------------------------CSS Noticias--------------------------------------------------------*/
</style>
<div class="site-index" style="margin: auto;">

    <!-- Jumbotron: un componente visual destacado en el centro de la página, sin contenido en este caso -->
    <div class="jumbotron text-center bg-transparent">
        
    </div>

    <div class="body-content">

        <div class="content">
            <div class="wrapper">
                <div class="content-box">

                    <div class="content-without-middle">
                        <div class="left-side the-article-content">

                            <!-- Encabezado principal de la sección de noticias -->
                            <div class="full-left-box">
                                <h1 class="specialnews">Noticias de Ciclismo</h1>
                            </div>

                            <!-- Contenido principal de la sección de noticias -->
                            <div class="article-main-content">
                                <div class="left-article-block article-background">
                                    <div class="article-content">

                                        <!-- Primer bloque de noticias -->
                                        <div class="especialnoticias">
                                            <?= Html::img('@web/images/noticias/aaron.jpg', ['alt' => 'Aaron Gate se impone con claridad en la primera etapa de la Clásica de Nueva Zelanda']) ?>
                                            <h2 class="font-replace"><b>Aaron Gate se impone con claridad en la primera etapa de la Clásica de Nueva Zelanda</b></h2>
                                            <p style="margin-right:5px;margin-left:5px;">El equipo Burgos-BH ha comenzado su temporada 2024 de manera espectacular, con una victoria en la primera etapa de la Clásica de Nueva Zelanda, cortesía de su nuevo fichaje, Aaron... <span class="texto8">(10/01/2024)</span></p>
                                        </div>

                                        <!-- Segundo bloque de noticias -->
                                        <div class="especialnoticias">
                                            <?= Html::img('@web/images/noticias/julian.jpg', ['alt' => 'Julian Alaphilippe confirma que disputará el próximo Giro de Italia y no el Tour']) ?>
                                            <h2 class="font-replace"><b>Julian Alaphilippe confirma que disputará el próximo Giro de Italia y no el Tour</b></h2>
                                            <p style="margin-right:5px;margin-left:5px;">El ciclista francés Julian Alaphilippe no participará en la próxima edición del Tour de Francia y sí en el Giro de Italia, según confirmó al diario L´Equipe. El doble campeón... <span class="texto8">(10/01/2024)</span></p>
                                        </div>

                                        <!-- Tercer bloque de noticias -->
                                        <div class="especialnoticias">
                                            <?= Html::img('@web/images/noticias/philips.jpg', ['alt' => 'Phil Bauhaus vuela en el salto del Lago di Garda y se lleva la victoria']) ?>
                                            <h2 class="font-replace"><b>Phil Bauhaus vuela en el salto del Lago di Garda y se lleva la victoria</b></h2>
                                            <p style="margin-right:5px;margin-left:5px;">El alemán Phil Bauhaus (Bahrain Victorious) se llevó la victoria en la segunda etapa de la Vuelta a Suiza al imponerse al sprint en el salto del Lago di Garda, una llegada... <span class="texto8">(11/01/2024)</span></p>
                                        </div>

                                        <!-- Cuarto bloque de noticias -->
                                        <div class="especialnoticias">
                                            <?= Html::img('@web/images/noticias/dylan.jpg', ['alt' => 'Dylan Groenewegen se impone en la Victoria de las Tres Cantonas']) ?>
                                            <h2 class="font-replace"><b>Dylan Groenewegen se impone en la Victoria de las Tres Cantonas</b></h2>
                                            <p style="margin-right:5px;margin-left:5px;">El ciclista neerlandés Dylan Groenewegen (Team BikeExchange) logró la victoria en la Victoria de las Tres Cantonas, segunda etapa de la Vuelta a Suiza, tras un sprint... <span class="texto8">(10/01/2024)</span></p>
                                        </div>

                                        <!-- Quinto bloque de noticias -->
                                        <div class="especialnoticias">
                                            <?= Html::img('@web/images/noticias/harold.jpg', ['alt' => 'Harold Tejada ficha por el equipo Kern Pharma']) ?>
                                            <h2 class="font-replace"><b>Harold Tejada ficha por el equipo Kern Pharma</b></h2>
                                            <p style="margin-right:5px;margin-left:5px;">El ciclista colombiano Harold Tejada, una de las jóvenes promesas del ciclismo, ha fichado por el equipo español Kern Pharma. Tejada, de 24 años, llega a este equipo... <span class="texto8">(09/01/2024)</span></p>
                                        </div><!-- Primer bloque de noticias -->
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
