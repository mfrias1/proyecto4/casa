<?php
use yii\helpers\Html;

$this->title = 'Inicio';
?>

<!-- Aquí iría tu código Yii2 para incluir el menú y otros elementos específicos de Yii2 -->

<!-- HTML y CSS Básico -->
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mi Sitio - <?= Html::encode($this->title) ?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <style>
        /* Personalización del temporizador */
        .jumbotron {
            background-color: #dc3545; /* Fondo rojo */
            color: #fff; /* Texto blanco */
        }

        .display-4,
        #countdown {
            text-align: center;
        }

        #countdown {
            font-size: 3em; /* Tamaño de fuente más grande */
            margin-top: 20px; /* Espacio superior */
        }

        /* Espaciado adicional para el carrusel */
        #myCarousel {
            margin-top: 20px;
        }
    </style>
</head>
<body>

<!-- Menú de Navegación -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Mi Sitio</a>
    <!-- ... (Resto del menú) ... -->
</nav>

<!-- Destacado y Temporizador -->
<div class="jumbotron">
    <h1 class="display-4">Próxima Temporada de Ciclismo</h1>
    <div id="countdown"></div>

    <script>
        // Configura la fecha de finalización del temporizador
        var endDate = new Date("April 16, 2024 00:00:00").getTime();

        // Actualiza el temporizador cada segundo
        var countdownInterval = setInterval(function() {
            var now = new Date().getTime();
            var timeLeft = endDate - now;

            if (timeLeft < 0) {
                clearInterval(countdownInterval);
                document.getElementById("countdown").innerHTML = "¡La temporada ha comenzado!";
            } else {
                var days = Math.floor(timeLeft / (1000 * 60 * 60 * 24));
                var hours = Math.floor((timeLeft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((timeLeft % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((timeLeft % (1000 * 60)) / 1000);

                document.getElementById("countdown").innerHTML = days + "d " + hours + "h " + minutes + "m " + seconds + "s ";
            }
        }, 1000);
    </script>
</div>

<!-- Carrusel con Bootstrap -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="https://via.placeholder.com/1200x400/FF5733/000000" class="d-block w-100" alt="Carrusel 1">
        </div>
        <div class="carousel-item">
            <img src="https://via.placeholder.com/1200x400/33FF57/000000" class="d-block w-100" alt="Carrusel 2">
        </div>
        <!-- Agrega más items del carrusel aquí -->
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Anterior</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Siguiente</span>
    </a>
</div>

<!-- Footer -->
<footer class="footer mt-auto py-3 bg-light">
    <div class="container">
        <span class="text-muted">© 2024 Mi Sitio</span>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.0.8/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</body>
</html>


