<?php

use app\models\lleva;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Llevas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lleva-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Lleva', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'dorsal',
            'numetapa',
            'código',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, lleva $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'numetapa' => $model->numetapa, 'código' => $model->código]);
                 }
            ],
        ],
    ]); ?>


</div>
